var realDays = [];

(function ($) {

  Drupal.behaviors.cdc = {
    attach: function (context) {
      for (var id in Drupal.settings.datePopup) {
        Drupal.settings.datePopup[id].settings.minDate = new Date();
        Drupal.settings.datePopup[id].settings.beforeShowDay = checkDate;
      }
    }
  };

  function checkDate(date) {
    var newDate = date.format("{Date:2}-{Month:2}-{FullYear}");
    if (jQuery.inArray( newDate, realDays ) != -1) {return [true];}
    else {return [false];}
  }

})(jQuery);

/***********************/
/* Format date function
/***********************/
Date.prototype.format = function (fmt) {

    var date = this;
    return fmt.replace(
      /\{([^}:]+)(?::(\d+))?\}/g,
      function (s, comp, pad) {
        var fn = date["get" + comp];
        if (fn) {
            var v = (fn.call(date) +
                (/Month$/.test(comp) ? 1 : 0)).toString();
            return pad && (pad = pad - v.length)
                ? new Array(pad + 1).join("0") + v
                : v;
        } else {
            return s;
        }
    });
};

/***********************/
/* Get base path
/***********************/
function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}
