<?php
/**
 * @file
 * Template file for the example display.
 *
 * Variables available:
 * 
 * $plugin: The pager plugin object. This contains the view.
 *
 * $plugin->view
 *   The view object for this navigation.
 *
 * $nav_title
 *   The formatted title for this view. In the case of block
 *   views, it will be a link to the full view, otherwise it will
 *   be the formatted name of the year, month, day, or week.
 *
 * $prev_url
 * $next_url
 *   Urls for the previous and next calendar pages. The links are
 *   composed in the template to make it easier to change the text,
 *   add images, etc.
 *
 * $prev_options
 * $next_options
 *   Query strings and other options for the links that need to
 *   be used in the l() function, including rel=nofollow.
 */
?>

<?php if (!empty($pager_prefix)) print $pager_prefix; ?>
  <div class="date-nav-wrapper clearfix<?php if (!empty($extra_classes)) print $extra_classes; ?>">
    <div class="date-nav item-list">
      <ul class="pager">
      <?php if (!empty($prev_url)) : ?>
         <li class="event__month__prev">
         <?php print l('<i class="fa fa-caret-left"></i>'.($mini ? '' : ' ' . t(ucfirst($prev_title), array(), array('context' => 'date_nav'))), $prev_url, $prev_options); ?>
         </li>
      <?php endif; ?>
      <li class="event__month__title"><?php echo ucfirst($nav_title); ?></li>
      <?php if (!empty($next_url)) : ?>
        <li class="event__month__next">
          <?php print l(($mini ? '' : t(ucfirst($next_title), array(), array('context' => 'date_nav')) . ' ') . '<i class="fa fa-caret-right"></i>', $next_url, $next_options); ?>
        </li>
      <?php else:?>
      <?php endif; ?>
      </ul>
    </div>
  </div>
