<?php if (!empty($page['highlighted'])): ?>
  <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
<?php endif; ?>
<?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
<a id="main-content"></a>
<?php print render($title_prefix);
$title_0 = 0;
if ( arg(0) == 'node' && is_numeric(arg(1)) ) {
  	$node = node_load(arg(1));
  	if($node->type == 'formation'){
  		$title_0 = 1;
	}
}
?>
<?php if (!empty($title) && ($title != 'Formations' && $title_0 != 1) ): ?>
  <h1 class="page-header"><?php print $title; ?></h1>
<?php endif; ?>
<?php print render($title_suffix); ?>
<?php print $messages; ?>
<?php if (!empty($tabs)): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
<?php if (!empty($page['help'])): ?>
  <?php print render($page['help']); ?>
<?php endif; ?>
<?php if (!empty($action_links)): ?>
  <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>