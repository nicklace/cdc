<section class="container formation-block__search">
      <?php //print render($page['header']); ?>
       <div class="row autocomplete autocomplete-processed">
        <div class="col-lg-offset-1 col-lg-5">
          <h2><?php echo t('Rechercher une formation'); ?></h2>
        </div>
      </div>
      <?php
        $arr = drupal_get_form('cdc_formations_search_form');
        print drupal_render($arr);
      ?>
</section> <!-- /#search-block -->