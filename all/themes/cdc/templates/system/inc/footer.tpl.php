  <footer class="footer">
    <address class="container footer__infos">
      <div class="row">
        <div class="col-sm-4 col-lg-3 col-lg-offset-1">
          <div class="footer__block">
            <span class="footer__icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
            <div class="footer__content">
              <?php echo config_pages_get('center_config','field_footer_address'); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-2 col-lg-offset-1">
          <div class="footer__block">
            <span class="footer__icon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
            <div class="footer__content">
              <b>Tél.</b> <?php echo config_pages_get('center_config','field_footer_phone'); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-3 col-lg-offset-2">
          <div class="footer__block">
          <span class="footer__icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
          <div class="footer__content">
            <a href="mailto:<?php echo config_pages_get('center_config','field_footer_email'); ?>"><?php echo config_pages_get('center_config','field_footer_email'); ?></a>
          </div>
        </div>
        </div>
      </div>
    </address>
    <div id="map" class="container footer__map" data-lat="<?php echo config_pages_get('center_config','field_latitude'); ?>" data-lng="<?php echo config_pages_get('center_config','field_longitude'); ?>">
    </div>
    <div class="footer__links container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-4 col-lg-offset-0 supporters">
          <a class="supporter__element plushaut" href="http://www.plushaut.be/" target="_blank">Plus Haut</a>
        </div>
        <div class="col-lg-6 col-lg-offset-2 links">
          <p>
            <a class="link__element" href="#" target="_blank">Vie privée</a> -
            <a class="link__element" href="#" target="_blank">Conditions d'utilisation</a>
            <small>&copy; Le Forem 2016</small>
          </p>
        </div>
      </div>
    </div>

<!-- Modal -->
    <div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Sélectionnez votre profil</h4>
          </div>
          <div class="modal-body">

           <div class="modal__col">
              <a href="<?php echo $current_path; ?>" class="profile__button" data-profile='demandeur'>
                <div class="modal__img modal__img_1">
                  <span class="modal__col__text">Demandeur d'emploi</span>
                </div>
              </a>
            </div>
            <div class="modal__col">
              <a href="<?php echo $current_path; ?>" class="profile__button" data-profile='entreprises'>
                <div class="modal__img modal__img_2">
                  <span class="modal__col__text">Entreprises et travailleurs</span>
                </div>
              </a>
            </div>
            <div class="modal__col">
              <a href="<?php echo $current_path; ?>" class="profile__button" data-profile='enseignement'>
                <div class="modal__img modal__img_3">
                  <span class="modal__col__text">Enseignement</span>
                </div>
              </a>
            </div>
           
          </div>
        </div>
      </div>
    </div>
</footer>

<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-583eb4a805ad333d"></script> 