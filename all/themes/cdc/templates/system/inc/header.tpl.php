<?php
global $base_url;
$current_path = $base_url.'/'.current_path();
?>
<header>
  <div class="<?php print $container_class; ?>">
    <div class="header__buttons">
      <div class="header__buttons-wrapper">
        <button type="button" class="buttons-button" data-toggle="collapse" data-target="#navProfile" aria-expanded="false" aria-controls="navProfile"><i class="fa fa-user"></i></button>
      </div>
      <div class="search-wrapper">
        <button type="button" class="search-button"><i class="fa fa-search"></i></button>
        <?php
          $block = module_invoke('search', 'block_view', 'search');
          print render($block);  
        ?>
      </div>
    </div>
  </div>
  <nav class="navbar">
    <div class="container">
      <div class="navbar-header">
        <?php if ($logo): ?>
          <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"></a>
        <?php endif; ?>

        <?php if (!empty($site_name)): ?>
          <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
        <?php endif; ?>

        <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <?php endif; ?>
      </div>

      <div id="navProfile" class="header__buttons-wrapper-inner collapse">
        <ul>
          <li><a href="<?php echo $current_path; ?>" class="btn btn__default profile__button" data-profile='demandeur'><i class="icon__demandeur_emploi"></i>Demandeurs d'emploi</a></li>
          <li><a href="<?php echo $current_path; ?>" class="btn btn__default profile__button" data-profile='entreprises'><i class="icon__entreprises_travailleurs"></i>Entreprises et travailleurs</a></li>
          <li><a href="<?php echo $current_path; ?>" class="btn btn__default profile__button" data-profile='enseignement'><i class="icon__enseignement"></i>Enseignement</a></li>
        </ul>
      </div>

      <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div id="navbar" class="collapse navbar-collapse col-lg-nopadding">
        <nav role="navigation">
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
        </nav>
      </div>
      <?php endif; ?>
    </nav>
</header>