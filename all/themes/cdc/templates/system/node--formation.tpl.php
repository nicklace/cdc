<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>

<?php
global $base_url;
$profile = $content['field_public_cible']["#items"][0]["value"]; 
?>

<article class="container fiche__formation">
	<header>
        <div class="fiche__formation-header">
          <div class="row">
            <div class="col-xs-12 col-md-8 fiche__formation-title">
              <div class="fiche__formation-header-btns">
	          	<a href="javascript:window.print()"><i class="fa fa-print"></i></a>
	          	<div class="addthis_inline_share_toolbox"></div>
	          </div>
              <!--<span class="fiche_formation-domain">Nom du domaine</span>-->
              <h1><?php 
              	$out = strlen($title) > 75 ? substr($title,0,75)."..." : $title;
              	print $out; ?></h1>
            </div>
            <a href="../<?php echo drupal_get_path_alias(current_path());?>/je-suis-interresse" class="btn btn__default btn__big fiche__formation-btn icon icon__arrow-white icon__right col-xs-12 col-md-4">Je suis interessé</a>
          </div>
        </div>
        <?php if($_COOKIE['profile']!=$profile){ ?>
        <div class="alert alert-danger alert-dismissible red-text" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Cette formation s'applique <span class="text-uppercase">uniquement</span> aux <strong class="text-uppercase"><?php print $content['field_public_cible'][0]["#markup"]; ?></strong>
        </div>
        <?php } ?>
    </header>
    <div class="row">
		<div class="col-sm-4">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php if($profile=='demandeur' && !empty($content['field_info_sessions'])) { ?>
				 <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingSession">
						<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSession" aria-expanded="true" aria-controls="collapseSession"><i class="fa fa-calendar-o"></i>Date et lieu de la séance d'information</a></h4>
					</div>
					<div id="collapseSession" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSession">
						<div class="panel-body">
						<?php
						foreach($content['field_info_sessions'] as $key=>$field_collection) {
						    if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) {
						    	$field_session_item = current($field_collection['entity']['field_collection_item']); ?>
								<p><?php print render($field_session_item['field_info_sessions_date']); ?><br>
								<?php if(!empty($field_session_item['field_info_sessions_more'])){ ?><strong class="red-text"><?php print render($field_session_item['field_info_sessions_more']); ?></strong></p><?php } ?>
								<strong><?php print render($field_session_item['field_info_sessions_place_place']); ?></strong><br>
								<?php print render($field_session_item['field_info_sessions_place']); ?>
						 	<?php 
						 	}
						 }?>
						</div>
					</div>	
				</div>	
				<?php } ?>  
            	<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingFormation">
						<h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFormation" aria-expanded="true" aria-controls="collapseFormation"><i class="fa fa-calendar-o"></i>Date et lieu de la formation</a></h4>
					</div>
					<div id="collapseFormation" class="panel-collapse collapse <?php if($profile!='demandeur'){?> in<?php }?>" role="tabpanel" aria-labelledby="headingFormation">
						<div class="panel-body">
							<p><?php print render($content['field_date']); ?><br>
								<?php if(!empty($content['field_more'])){ ?><strong class="red-text"><?php print render($content['field_more']); ?></strong></p><?php } ?>
								<strong><?php print render($content['field_place_title']); ?></strong><br>
								<?php print render($content['field_place']); ?> 
						</div>
					</div>	
				</div>
          	</div>
          	<ul class="fiche__formation-details">
				<li class="fiche__formation-detail">
					<h3 class="fiche__formation-detail-title"><i class="fa fa-group"></i>Public cible</h3>
					<p><?php print render($content['field_public_cible']); ?></p>
					<?php if(!empty($content['field_public_cible_more'])){ ?><strong class="red-text"><?php print render($content['field_public_cible_more']); ?></strong></p><?php } ?>
				</li>
				<li class="fiche__formation-detail">
					<h3 class="fiche__formation-detail-title"><i class="fa fa-briefcase"></i>Type</h3>
					<p><?php print render($content['field_lenght']); ?></p>
				</li>
				<li class="fiche__formation-detail">
					<h3 class="fiche__formation-detail-title"><i class="fa fa-calendar"></i>Durée et horaires</h3>
					<?php print render($content['field_timing']); ?>
				</li>
				<li class="fiche__formation-detail">
					<h3 class="fiche__formation-detail-title"><i class="fa fa-euro"></i>Prix</h3>
					<?php if($content['field_free'][0]["#markup"]==0){ ?>
						<p><?php 
						print render($content['field_price']); ?>€</p>
						<?php if(!empty($content['field_price_more'])){ ?><strong class="red-text"><?php print render($content['field_price_more']); ?></strong></p><?php } ?>
					<?php }else{ ?>
						<img class="img-responsive" src="<?php echo $base_url ?>/sites/all/themes/cdc/img/icon_formation_gratuite.png" alt="Badge formation gratuite">
					<?php } ?>
				<li class="fiche__formation-detail">
					<h3 class="fiche__formation-detail-title"><i class="fa fa-phone"></i>Contact et inscription</h3>
					<?php print render($content['field_contact']); ?>
				</li>
			</ul>
        </div>

        <div class="col-sm-8 fiche__formation-content">
          	<div class="row">
            	<div class="<?php echo (!empty($content['field_content_image']) ? 'col-lg-6' : 'col-lg-12'); ?>">
	              	<div class="fiche__formation-description">
	                	<h3>Description</h3>
	                	<?php print render($content['field_description']); ?>
	              	</div>
              		<div class="gray__block hidden-xs hidden-sm pull-right"></div>
           
            	<?php if(!empty($content['field_content_image']) ){ ?>
        		<div class="col-lg-6">
        		<?php } ?>	
        			<?php if(!empty($content['field_reference']) ){ ?>
              		<span class="fiche__formation-description-code">Code <?php print render($content['field_reference']); ?></span>
              		<?php } ?>	
              	<?php if(!empty($content['field_content_image']) ){ ?>	
              		<div class="gray__block hidden-xs hidden-sm"></div>
              		<div>
						<?php print render($content['field_content_image']); ?>
              		</div>
            	</div>
            	<?php } ?>
          	</div>
			<div class="col-lg-12">
           <?php print render($content['body']); ?>
			</div>
        </div>
     </div>
</article>
 <div class="container">
	<div class="row fiche__formation-btns">
		<a href="#" onclick="window.history.back(); return false;" class="btn btn__big btn__gray col-sm-8 fiche__formation-btn icon icon__arrow-reverse">Retour aux résultats de recherche</a>
		<a href="../<?php echo drupal_get_path_alias(current_path());?>/je-suis-interresse" class="btn btn__default btn__big col-sm-4 fiche__formation-btn icon icon__arrow-white icon__right">Je suis interessé</a>
	</div>
  	<h3 class="titleMargin">Des formations qui pourraient vous intéresser</h3>
  	<?php 
	$block = block_load('views', 'formations-block_upcoming');
	$output = _block_get_renderable_array(_block_render_blocks(array($block)));
	$output = drupal_render( $output );        
	print $output;
	?>
</div>

