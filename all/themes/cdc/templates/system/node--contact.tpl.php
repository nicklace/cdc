<div class="row">
    <div class="col-lg-6 col-lg-push-6 contact__text">
		<?php print render($content); ?>
    </div>
     <div class="col-lg-6 col-lg-pull-6 ">
      <div id="map" style="position: relative; overflow: hidden; width:100%; height:500px;" data-lat="<?php echo config_pages_get('center_config','field_latitude'); ?>" data-lng="<?php echo config_pages_get('center_config','field_longitude'); ?>"></div>
    </div>
</div>

<div class="contact-form"><bR>
<h3>Formulaire de contact</h3>
        <p>Afin de nous permettre de traiter le plus efficacement possible votre requête, veuillez nous donner un maximum d’informations. Les champs marqués d’un astérisque (*) sont obligatoires.</p>
        <form method="post" id="contactForm">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="lastname">Nom</label>
                <input class="form-control" required="" id="lastname" name="lastname" type="text">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="firstname">Prénom</label>
                <input class="form-control" required="" id="firstname" name="firstname" type="text">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="number">Numéro forem</label>
                <input class="form-control" required="" id="number" name="number" type="text">
                <p class="help-block">Ce numéro est inscrit sur votre carte jobpass</p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="select">Résident en Belgique</label>
                <select class="select form-control" required="" id="select" name="select">
                 <option value="First Choice">
                  First Choice
                 </option>
                 <option value="Second Choice">
                  Second Choice
                 </option>
                 <option value="Third Choice">
                  Third Choice
                 </option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="email">Téléphone / Gsm</label>
                <input class="form-control" required="" id="email" name="email" type="tel">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="control-label" for="email">Email</label>
                <input class="form-control" required="" id="email" name="email" type="email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="subject">Veuillez décrire le motif de votre demande</label>
                <textarea class="form-control" id="subject" name="subject" rows="5" ></textarea> 
                <p class="help-block">Si vous signalez un problème technique, veuillez préciser le type de navigateur utilisé ainsi que le mesage d’erreur obtenu</p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="exampleInputFile">Vous pouvez joindre un document à votre demande</label>
                <div class="file-upload-wrapper" data-text="Selectionner votre fichier">
                  <input name="file-upload-field" type="file" class="file-upload-field" value="">
                </div>
                <p class="help-block">Uniquement des documents de type word, pdf, images, taille maximum 5Mb</p>
              </div>


            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <input type="submit" class="btn btn__big btn__default" name="submit" value="Envoyer">
              </div>
            </div>
          </div>
        </form>
      </div>