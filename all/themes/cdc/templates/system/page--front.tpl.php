<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
?>
<?php include('inc/header.tpl.php'); ?>

<?php include('inc/search_block.tpl.php'); ?>

<section class="container formation-block__agenda">
    <div class="row">
      <div class="col-lg-6 col-sm-8 col-lg-offset-1">
        <?php 
          if($_COOKIE['profile']=='demandeur'){ 
            $block = block_load('views', 'calendar-bck_home_calendar_session');
          }else{
            $block = block_load('views', 'calendar-bck_home_calendar');
          }
          $output = _block_get_renderable_array(_block_render_blocks(array($block)));
          $output = drupal_render( $output );        
          print $output;
        ?>
        <a href="/calendrier" class="btn btn__default featured__button">Voir l'agenda complet</a>
      </div>
      <div class="col-lg-3 col-sm-4 col-lg-offset-1 col-lg-nopadding hidden-xs formation-agenda__about">
        <h2>A propos</h2>
        <div class="formation-block__element">
          <?php echo config_pages_get('center_config','field_about_txt');?> 
          <a href="<?php echo config_pages_get('center_config','field_about_link');?>" class="arrow__link">Lire plus</a>
        </div>
      </div>
    </div>
  </section>
  <main>
   <?php 
      $title = config_pages_get('center_config','field_banner_title');
      $content = config_pages_get('center_config','field_banner_content');
      $date = config_pages_get('center_config','field_banner_date');
      $link = config_pages_get('center_config','field_banner_button_link');
      $label = config_pages_get('center_config','field_banner_button_label');
      $target = config_pages_get('center_config','field_banner_button_target');
      $now = time();
      if(isset($title) && isset($content) && ($date['value2'] > $now && $date['value'] < $now)){
    ?>
    <section class="container content-box">
      <div class="row">
        <div class="col-sm-8 col-lg-7 col-lg-offset-1">
          <h2><?php echo $title; ?></h2>
          <?php echo $content; ?>
        </div>
        <div class="col-sm-4">
          <?php if(isset($link) && isset($label)){ ?>
          <br><br><br><a href="<?php echo $link; ?>" target="<?php echo $target; ?>" class="btn btn__big btn__default"><?php echo $label; ?></a>
          <?php } ?>
        </div>
      </div>
    </section>
    <?php } ?>

    <section class="news">
      <div class="container">
        <h2 class="visible-xs-block">Actualités</h2>
        <div class="row news__home">
          <?php
            $block = block_load('views', 'articles-bk_home_featured');
            $renderable_array = _block_get_renderable_array(_block_render_blocks(array($block)));
            $output = drupal_render( $renderable_array );
            print $output;
          ?>
          <?php
            $block = block_load('views', 'articles-bk_home');
            $renderable_array = _block_get_renderable_array(_block_render_blocks(array($block)));
            $output = drupal_render( $renderable_array );
            print $output;
          ?>
          <div class="col-lg-4 news__images visible-lg">
            <div class="row">
              <div class="col-lg-8 news__square"><img src="<?php echo $base_url ?>/sites/all/themes/cdc/img/centres/<?php echo $domain_class; ?>/news_feature_img_1.jpg" alt="" class="img-responsive"></div>
            </div>
            <div class="row">
              <div class="col-lg-8 col-lg-offset-4 news__square square__gray"><img src="<?php echo $base_url ?>/sites/all/themes/cdc/img/centres/<?php echo $domain_class; ?>/news_feature_img_2.jpg" alt="" class="img-responsive"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.container -->

<?php include('inc/footer.tpl.php'); ?>