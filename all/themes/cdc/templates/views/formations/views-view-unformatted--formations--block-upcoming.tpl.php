<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="slider__wrapper">
    <div class="swiper-container">
     	<div class="swiper-wrapper">
			<?php foreach ($rows as $id => $row): ?>
		  	<div class="swiper-slide">
		    	<?php print $row; ?>
		  	</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div class="swiper-button swiper-button-next"></div>
    <div class="swiper-button swiper-button-prev"></div>
</div>