<?php 
$date = ($fields['field_public_cible']->content == 'Demandeur d\'emploi') ? $fields['field_info_sessions_field_info_sessions_date_value']->content : $fields['field_date']->content ; 
$format = 'd/m/Y - H:i';
$realDate = DateTime::createFromFormat($format, $date);
?>
<div class="formation__recommanded-element">
	<div class="date__infos">
		<span class="date__day"><?php echo $realDate->format('d'); ?></span>
		<span class="date__month"><?php echo t($realDate->format('F')); ?></span>
	</div>
	<h4><?php print $fields['title']->content; ?></h4>
	<p><?php print $fields['field_lenght']->content; ?></p>
	<a href="<?php print $fields['url']->content; ?>" class="arrow__link">Lire plus</a>
</div>