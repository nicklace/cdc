<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<a href="<?php print $fields['url']->content; ?>" >
  <h3><?php print $fields['title']->content; ?></h3>
  <span class="summary__cat">Formation pour <?php print $fields['field_public_cible']->content; ?></span>
  <div class="summary__infos">
  	<?php $date = ($fields['field_public_cible']->content == 'Demandeur d\'emploi') ? 'Séance d\'information : '.$fields['field_info_sessions_field_info_sessions_date_value']->content : $fields['field_date']->content ; ?>
    <span class="summary__infos-date"><i class="fa fa-calendar-o"></i><?php echo $date; ?></span>
    <span class="summary__infos-place"><i class="fa fa-map-marker"></i><?php print $fields['field_place_title']->content; ?></span>
  </div>
  <div class="summary__content">
    <span class="summary__content-title">Description</span>
    <p><?php print $fields['field_description']->content; ?>
    </p>
  </div>
</a>
