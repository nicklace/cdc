<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="row">
<?php foreach ($rows as $id => $row): ?>
  	<div class="col-md-6">
        <article class="formation__summary">
    		<?php print $row; ?>
    	</article>	
  </div>
<?php endforeach; ?>
</div>