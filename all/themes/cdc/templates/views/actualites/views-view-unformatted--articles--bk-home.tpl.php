<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<ul>
  <?php foreach ($rows as $id => $row): ?>
  <li>
    <article class="news__element">
    	<?php print $row; ?>
    </article>
  </li>
  <?php endforeach; ?>
</ul>
