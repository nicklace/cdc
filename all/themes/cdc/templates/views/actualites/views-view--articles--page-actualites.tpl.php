<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

  <?php 
  $twitter = config_pages_get('center_config','field_twitter');
  if ($twitter<>''){ ?>
  
  <div class="twitter-timeline">
    <h2>Twitter</h2>
    <a href="https://twitter.com/<?php echo $twitter; ?>" target="_blank">Nous suivre sur twitter</a>
    <div class="row" id="twitterfeed"> 

    </div>
  </div>
  <script type="text/javascript">
    jQuery('#twitterfeed').twittie({
      username: '<?php echo $twitter; ?>',
      template: '<div class="col-md-4"><div class="tweet"><div class="tweet-body"><div class="tweet-header"><h4 class="user-name">{{screen_name}}</h4> <time class="timestamp"><span class="field-content"> {{date}}</span></time></div><p class="tweet-text"><span class="field-content">{{tweet}}</span></p></div></div></div>',    
      apiPath : 'sites/all/themes/cdc/api/tweet.php',
      loadingText: 'Chargement...',
      count: 3,
      dateFormat: '%d/%m/%Y'
    });
  </script>
  <?php } ?>
</div><?php /* class view */ ?>


