<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="row news__page-listing">  
  <?php foreach ($rows as $id => $row): ?>
	<div class="col-md-6">
	  <article class="news__element">
	    <?php print $row; ?>
	  </article>  
	</div>
  <?php endforeach; ?>
</div>