<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php foreach ($rows as $id => $row): ?>
<article class="col-sm-6 col-lg-4 news__element news__feature">
    <?php print $row; ?>
</article>
<?php endforeach; ?>