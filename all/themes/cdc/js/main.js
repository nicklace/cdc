(function($) {

  jQuery(document).ready(function() {

    /* Manage cookie for profile */
    $profile = Cookies.get('profile');
    $setted = Cookies.get('profile-setted');

    if (typeof($setted) == "undefined" && !$('body').hasClass('page-user')) {
      $('#profileModal').modal('show');
    }

    $('.reset').on('click', function() {
      $('#cdc-formations-search-form input').each(function(){
        $(this).val('');
        $('.chosen-single').children('span').html('Domaines');
      });
      return false;
    })

    $('*[data-profile=' + $profile + ']').addClass('active');

    $('#edit-forem').formatter({
      'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}'
    });

    $('.profile__button').on('click', function() {
      $profile_choosed = $(this).data('profile');
      $target = $(this).attr('href');
      Cookies.set('profile', $profile_choosed)
      Cookies.set('profile-setted', 1, { expires: 30 });
      window.location.href = $target;

      return false;
    })

    var getFormationsDates = $.ajax({
      timeout: 0,
      cache: true,
      url: window.location.protocol + "//" + window.location.host + "/" + 'formations/json',
      dataType: "json",
      type: "GET",
      success: function(data) {
        data.forEach(function(object) {
          if ($profile == 'demandeur') {
            realDays.push(object.date_seance);
          } else {
            realDays.push(object.date_formation);
          }
        });
      }
    });

    /* Manage search form */
    $('.search-button').click(function() {
      $('#search-block-form').toggleClass('active');
    })

    $('.buttons-button').click(function() {
      $('#search-block-form').removeClass('active');
    })

    /* ===== Chosen ====== */
    $("#edit-domaines").chosen();

    /* ===== Contact upload ====== */
    $("form").on("change", ".file-upload-field", function() {
      $(this).parent(".file-upload-wrapper").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ''));
    });


    /* ===== Swiper ====== */
    var slidesPerView = 4;
    var spaceBetween = 24;
    if($( window ).width() < 992 ) {
      slidesPerView = 2;
      spaceBetween = 12;
    }
    if($( window ).width() < 768 ) {
      slidesPerView = 1;
      spaceBetween = 0;
    }

    if ($('.swiper-container').length) {
      var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: slidesPerView,
        spaceBetween: spaceBetween
      });
    };


    /* ===== Scroll on mobile ====== */
    if($( window ).width() < 992 && !urlParam('motscles')) {
      setTimeout( function() {
        scrollMe("#search__results");
      }, 800);
    }

    function urlParam(name) {
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);

      if (results==null){
        return null;
      }
      else{
        return results[1] || 0;
      }
    }

    function scrollMe(target){
      targetOffset = $(target).offset();
      $("html, body").animate({ scrollTop: targetOffset.top }, "slow");
    }

  });// end ready

})(jQuery);

var map;

function initMap() {
  var maper = document.getElementById('map');
  var myLatlng = new google.maps.LatLng(parseFloat(maper.dataset.lat),parseFloat(maper.dataset.lng));
  var mapOptions = {
    zoom: 17,
    center: myLatlng
  }
  var image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkUzQTVEQzFERTIxOTExRTZCNDMwQjg5OEM2MUM3MkE2IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkUzQTVEQzFFRTIxOTExRTZCNDMwQjg5OEM2MUM3MkE2Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTNBNURDMUJFMjE5MTFFNkI0MzBCODk4QzYxQzcyQTYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTNBNURDMUNFMjE5MTFFNkI0MzBCODk4QzYxQzcyQTYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7Ze+VaAAADrklEQVR42tSZWUhUURjHzx1npnHMLINMLDMK2n2oCHop8qHorQ1annspqYfCqIyKgmiXNqqHaEFaKaQFisCwlaISpCgIK8pUos1y1Flv/4NfYDLNOffcc6bxD78HxzvnfP97tu87Y9m2zVJp7vUH7GZjE2M+LxNoCJhNTAbDQC79rx18AvXgNrgFWlK2FouzGcUFrG7ezJSPCaOS0BiwEiwERf94ZgAxDiyj4GvAEfDSTeceF98NgO3gGVidIvhkKgQrwFOwG+Sk28AoUAs2uemcXkIFqKPRSYuBUnAHTGf6NIXanGbawEhwAwxn+lUAroGxpgzw4T5Pu4sp8Z3sIuhvwsBGlSFW0CSwTbeB0bTY0qVVYKJOA2toCjlViHAqr+wLkzGQDxY76DwKjoFZtDXyRcmP00Ogy0E7C+i8cH0Sl5EJGbWQ2Xu9Pkcuwu6CalqkIyTa4gt5DjjlagQsyyqTDD4MFiUJvqeegPmy00qmb6GBH5FIKVqS6Y+/qYcSz9XTFBNFz9rC0QlINS1lA+FEwt8c6ipkHqm1Xu1gfp8FtshAS2d4aHs0FlQ20BGLB0LRmEyu0wHeOjDwng9uagNoFMGHovGg211IRrbwjarKcrEGgt6srhyfV2bB8VEqcRAWz6XyRK8k6MvqQAydygb6eTyRopxAM0skZIJa6sDAEuHoo1IsDAZacwUvUDiFBvr9DcyWmh3LKS0WaTxVcExkIM/ve2EJpqbQQMK2ayXfaja4AqYKaokaKi8lPIj7ljmJeaHxFQyWeLaYqqujdOK+o8VdQjVzeY9CX6RfVPy7NvCN6oByyY75treW+E4G8hX2Hz6arbqy0SrQqRDEIMXgeUK4R2c63Qj2p7EeOCx73eLkINtJc9q0mum6RntJ2U5lpWltobVj5FbiAuX1psQvuk6avFbhO8o6fjwYMsDbjps0wPUYnDYQ/CU6c5hpA1ybQZvG4Hk6vkHli6oGmmhX0qUq2qrTZoDrIHijIfiPrPuGmqXbgPKw9xK/4f75PwxwXWbd1+yqeuSwljZSUlY43fp0bsk6DDwHJxS+dw7cd9u5rqJ+K6XdTtKSSh0d6zLArxR3OHh+L12tZIwBLv6L4yuJ53hGu09XpzoN8Jvn9RLPVdIUyjgDXFcFdew9Kk9Zphr4k1FGk11w0JZrZ7qBBnA8yednKJNlmW6AUUn4pcffbZTBsr5i4DP7+5fGXZS09RkDjKbRa/ABHDDVideggQgV6NmUuRrRbwEGAPPR2fjCiBKKAAAAAElFTkSuQmCC';
  var map = new google.maps.Map(maper, mapOptions);
  var marker = new google.maps.Marker({
    position: myLatlng,
    icon: image
  });

  marker.setMap(map);
};
  


/* ================================================
      UTILS
================================================ */

/**
* JavaScript Cookie v2.1.3
* https://github.com/js-cookie/js-cookie
*
* Copyright 2006, 2015 Klaus Hartl & Fagner Brack
* Released under the MIT license
*/

(function(factory) {
  var registeredInModuleLoader = false;
  if (typeof define === 'function' && define.amd) {
    define(factory);
    registeredInModuleLoader = true;
  }
  if (typeof exports === 'object') {
    module.exports = factory();
    registeredInModuleLoader = true;
  }
  if (!registeredInModuleLoader) {
    var OldCookies = window.Cookies;
    var api = window.Cookies = factory();
    api.noConflict = function() {
      window.Cookies = OldCookies;
      return api;
    };
  }
}(function() {
  function extend() {
    var i = 0;
    var result = {};
    for (; i < arguments.length; i++) {
      var attributes = arguments[i];
      for (var key in attributes) {
        result[key] = attributes[key];
      }
    }
    return result;
  }

  function init(converter) {
    function api(key, value, attributes) {
      var result;
      if (typeof document === 'undefined') {
        return;
      }

      // Write

      if (arguments.length > 1) {
        attributes = extend({
          path: '/'
        }, api.defaults, attributes);

        if (typeof attributes.expires === 'number') {
          var expires = new Date();
          expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
          attributes.expires = expires;
        }

        try {
          result = JSON.stringify(value);
          if (/^[\{\[]/.test(result)) {
            value = result;
          }
        } catch (e) {}

        if (!converter.write) {
          value = encodeURIComponent(String(value))
            .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
        } else {
          value = converter.write(value, key);
        }

        key = encodeURIComponent(String(key));
        key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
        key = key.replace(/[\(\)]/g, escape);

        return (document.cookie = [
          key, '=', value,
          attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
          attributes.path ? '; path=' + attributes.path : '',
          attributes.domain ? '; domain=' + attributes.domain : '',
          attributes.secure ? '; secure' : ''
        ].join(''));
      }

      // Read

      if (!key) {
        result = {};
      }

      // To prevent the for loop in the first place assign an empty array
      // in case there are no cookies at all. Also prevents odd result when
      // calling "get()"
      var cookies = document.cookie ? document.cookie.split('; ') : [];
      var rdecode = /(%[0-9A-Z]{2})+/g;
      var i = 0;

      for (; i < cookies.length; i++) {
        var parts = cookies[i].split('=');
        var cookie = parts.slice(1).join('=');

        if (cookie.charAt(0) === '"') {
          cookie = cookie.slice(1, -1);
        }

        try {
          var name = parts[0].replace(rdecode, decodeURIComponent);
          cookie = converter.read ?
            converter.read(cookie, name) : converter(cookie, name) ||
            cookie.replace(rdecode, decodeURIComponent);

          if (this.json) {
            try {
              cookie = JSON.parse(cookie);
            } catch (e) {}
          }

          if (key === name) {
            result = cookie;
            break;
          }

          if (!key) {
            result[name] = cookie;
          }
        } catch (e) {}
      }

      return result;
    }

    api.set = api;
    api.get = function(key) {
      return api.call(api, key);
    };
    api.getJSON = function() {
      return api.apply({
        json: true
      }, [].slice.call(arguments));
    };
    api.defaults = {};

    api.remove = function(key, attributes) {
      api(key, '', extend(attributes, {
        expires: -1
      }));
    };

    api.withConverter = init;

    return api;
  }

  return init(function() {});
}));



